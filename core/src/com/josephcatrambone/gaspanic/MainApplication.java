package com.josephcatrambone.gaspanic;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MainApplication extends ApplicationAdapter {
	public static AssetManager assetManager;
	public static ScreenManager screenManager;
	
	@Override
	public void create () {
		assetManager = new AssetManager();
		screenManager = new ScreenManager(null);
	}

	@Override
	public void render() {
		//Gdx.gl.glClearColor(1, 0, 0, 1);
		//Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		screenManager.render(Gdx.graphics.getDeltaTime());
	}
	
	@Override
	public void dispose() {
		screenManager.dispose();		
		assetManager.dispose(); // Remove all remaining assets.
	}

	@Override
	public void pause() {
		screenManager.pause();
	}

	@Override
	public void resize(int width, int height) {
		screenManager.resize(width, height);
	}

	@Override
	public void resume() {
		screenManager.resume();
	}
}
