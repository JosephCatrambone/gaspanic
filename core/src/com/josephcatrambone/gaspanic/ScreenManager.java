package com.josephcatrambone.gaspanic;

import java.util.Stack;

import com.badlogic.gdx.Screen;

public class ScreenManager {
	private Stack <Screen> stack;
	
	public ScreenManager(Screen startScreen) {
		stack = new Stack<Screen>();
		stack.push(startScreen);
	}
	
	public void dispose() {
		while(!stack.isEmpty()) {
			Screen s = stack.pop();
			s.hide();
			s.dispose();
		}
	}
	
	public void pause() {
		if(stack.isEmpty()) { return; }
		stack.peek().pause();
	}
	
	public void resume() {
		if(stack.isEmpty()) { return; }
		stack.peek().resume();
	}
	
	public void render(float deltaTime) {
		if(stack.isEmpty()) { return; }
		stack.peek().render(deltaTime);
	}
	
	public void resize(int width, int height) {
		if(stack.isEmpty()) { return; }
		
		// NOTE: Unlike pause, resize has to happen all the way down the stack.
		// We'll pop them all off the stack into a NEW stack, then do a pop from that (while calling resize) to maintain the right order.
		// Note that if this is run asynchronously (which should never happen, in theory,) another reference doing a draw may go crazy.
		Stack <Screen> temp = new Stack<Screen>();
		while(!stack.isEmpty()) {
			temp.push(stack.pop());
		}
		while(!temp.isEmpty()) {
			Screen s = stack.pop();
			s.resize(width, height);
			stack.push(s);
		}
	}
	
	// Stack modification methods
	public void push(Screen screen) {
		if(!stack.isEmpty()) {
			stack.peek().hide();
		}
		screen.show();
		stack.push(screen);
	}
	
	public Screen pop() {
		Screen s = stack.pop();
		s.hide();
		if(!stack.isEmpty()) {
			stack.peek().show();
		}
		return s;
	}
	
	public Screen peek() {
		return stack.peek();
	}
	
	public Screen swap(Screen screen) {
		Screen prev = null;
		if(!stack.isEmpty()) {
			prev = stack.pop();
		}
		prev.hide();
		screen.show();
		stack.push(screen);
		return prev;
	}
}
