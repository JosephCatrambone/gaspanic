package com.josephcatrambone.gaspanic;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Entity {
	// Physics constructs
	private Vector2 position;
	private Vector2 velocity; // Velocity is a stand-in for rotation.
	private Vector2 acceleration;
	private Vector2 maxSpeed;
	private Rectangle hitbox;
	
	// Animation constructs
	private Texture spriteSheet;
	private Animation currentAnimation;
	private float stateTimeAccumulator = 0;
	
	
	
	public Entity() {
		
	}
	
	public void act(float dt) {
		
	}

	public void draw(Batch batch, float alpha) {
		if(spriteSheet != null) {
			TextureRegion reg = currentAnimation.getKeyFrame(stateTimeAccumulator);
			batch.draw(reg, position.x - (reg.getRegionWidth()>>2), position.y - (reg.getRegionHeight()>>2));
			currentAnimation.getKeyFrames()
		}
	}
}
